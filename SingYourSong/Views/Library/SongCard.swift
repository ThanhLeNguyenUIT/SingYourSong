//
//  SongCard.swift
//  SingYourSong
//
//  Created by Thành Nguyễn Lê on 03/11/2021.
//

import SwiftUI

struct SongCard: View {
    var body: some View {
        VStack{
            HStack{
                Spacer()
                Image("songbanner")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 70, height: 70)
                    .clipped()
                Spacer()
                VStack{
                    Spacer(minLength: 15)
                    Text("Wikinson UK")
                    Spacer()
                    Text("Song Name")
                    Text("Wikinson UK")
                    Spacer(minLength: 10)
                }
                Spacer(minLength: 150)
                VStack{
                    Spacer()
                    Image(systemName: "ellipsis")
                    Spacer(minLength: 40)
                    Text("4:27")
                    Spacer()
                }
                Spacer()
            }
        }
        .foregroundColor(Color.white)
        .frame(width: .infinity, height: 100)
        .background(Color("ItemColor"))
    }
}

struct SongCard_Previews: PreviewProvider {
    static var previews: some View {
        SongCard()
            .previewLayout(.fixed(width: 375, height: 150))
    }
}
