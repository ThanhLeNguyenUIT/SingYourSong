//
//  LibraryCardModel.swift
//  SingYourSong
//
//  Created by Thành Nguyễn Lê on 24/10/2021.
//

import Foundation
import SwiftUI

struct LibraryCardModel: Identifiable{
    var id = UUID().uuidString
    var icon: String
    var title: String
    var destination: AnyView
}

var lstLibraryCard = [
    LibraryCardModel(icon: "text.badge.plus", title: "Playlists & Albums", destination: AnyView(Playlist())),
    LibraryCardModel(icon: "waveform", title: "Uploaded Beat", destination: AnyView(Upload())),
    LibraryCardModel(icon: "heart.fill", title: "Liked Track", destination: AnyView(LikedTrack())),
]
