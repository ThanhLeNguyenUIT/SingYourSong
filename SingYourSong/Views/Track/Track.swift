//
//  Track.swift
//  SingYourSong
//
//  Created by Thành Nguyễn Lê on 08/11/2021.
//

import SwiftUI

struct Track: View {
    var body: some View {
        ZStack {
            
            Color("ItemColor")
                            .edgesIgnoringSafeArea(.all)
            
            VStack{
                HeaderView(title: "Bài hát", destiantion: AnyView(HomeView()))
                                        .padding(.bottom, 5)
                                      .background(Color.black)

                VStack{
                    ScrollView(.vertical){
                        TrackCard()
                        TrackCard()
                    }
                }
                .frame(
                      minWidth: 0,
                      maxWidth: .infinity,
                      minHeight: 0,
                      maxHeight: .infinity,
                      alignment: .topLeading
                    )
                .background(Color("ItemColor"))
            }
        }
    }
}

struct Track_Previews: PreviewProvider {
    static var previews: some View {
        Track()
    }
}
