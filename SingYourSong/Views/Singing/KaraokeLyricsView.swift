//
//  KaraokeLyricsView.swift
//  SingYourSong
//
//  Created by Tran Quoc Thinh on 29/11/2021.
//

import SwiftUI

struct KaraokeLyricsView: View {
    var content: LyricsSentence
    
    
    let columns: [GridItem] = [
        GridItem(.flexible(minimum: 100, maximum: 150), spacing: nil, alignment: nil ),
        GridItem(.flexible(minimum: 100, maximum: 150), spacing: nil, alignment: nil ),
        GridItem(.flexible(minimum: 100, maximum: 150), spacing: nil, alignment: nil ),
        GridItem(.flexible(minimum: 100, maximum: 150), spacing: nil, alignment: nil ),
        
        
    ]
    
    var body: some View {
        LazyVGrid(columns: columns, spacing: 5) {
            ForEach(self.content.words, id: \.self){ lyricsWord in
                KaraokeElementView(lyricsWord: lyricsWord)
            }
        }
        .frame(maxWidth: 400, maxHeight: 200)
        .padding(.top)
        .background(Color("Background"))
        
//        //** Fix input lyrics
//        HStack(alignment: .center, spacing: 5) {
//            ForEach(self.content.words, id: \.self) { lyricsWord in
//                KaraokeElementView(lyricsWord: lyricsWord)
//            }
//        }
//        .frame(maxWidth: .infinity, maxHeight: .infinity)
//        .background(Color("Background"))
    }
}

struct KaraokeLyricsView_Previews: PreviewProvider {
    static var previews: some View {
        KaraokeLyricsView(content: LyricsSentence(words: [
            LyricsWord(startTime: 100, endTime: 120, data: "Mua"),
            LyricsWord(startTime: 120, endTime: 140, data: "Xuan"),
            LyricsWord(startTime: 140, endTime: 160, data: "Vua")
        ]))
            .environmentObject(TimingObject())
//            .previewLayout(.sizeThatFits)
    }
        
}
