//
//  HomeView.swift
//  SingYourSong
//
//  Created by Tran Quoc Thinh on 20/10/2021.
//

import SwiftUI

struct HomeView: View {
    @EnvironmentObject var songTypeViewModel: SongTypeViewModel
    
    var body: some View {
        
        ZStack {
            
            Color("ItemColor")
                .edgesIgnoringSafeArea(.all)
            
            VStack{
                
                HeaderView(title: "Trang chủ", destiantion: AnyView(HomeView()))
                    .padding(.bottom, 5)
                    .background(Color.black)
                
                ScrollView {
                    VStack(spacing: 20) {
                        ForEach(songTypeViewModel.items) { item in
                            MusicTypeView(songTypeModel: item)
                                .padding(.leading, 10)
                                .shadow(color: Color.black.opacity(0.35), radius: 0, x: 4, y: 4)
                                .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .top)
                        }
                    }
                }
                .background(Color("Background"))
                
                FooterView()
                    .environmentObject(songTypeViewModel)
                    .padding(.top, 5)
                    .background(Color.black)
                
            }
        }
        
    }
    
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
        .environmentObject(SongTypeViewModel())
    }
}
