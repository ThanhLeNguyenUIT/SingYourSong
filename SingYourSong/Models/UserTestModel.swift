//
//  UserTestModel.swift
//  SingYourSong
//
//  Created by Thành Nguyễn Lê on 28/11/2021.
//

import Foundation

struct UserTestModel: Identifiable{
    let id: String
    let email: String
    let name: String
    let password: String
}
