//
//  TrackCard.swift
//  SingYourSong
//
//  Created by Thành Nguyễn Lê on 08/11/2021.
//

import SwiftUI

struct TrackCard: View {
    var body: some View {
        VStack{
            HStack{
                Spacer()
                Image("songbanner")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 70, height: 70)
                    .clipped()
                Spacer()
                VStack{
                    Spacer(minLength: 10)
                    Text("Wikinson UK")
                        .padding(.top,5)
                    Spacer(minLength: 5)
                    Text("Song Name")
                    Spacer(minLength: 5)
                    Text("Wikinson UK")
                        .padding(.bottom,5)
                    Spacer(minLength: 10)
                }
                Spacer(minLength: 150)
                VStack{
                    Spacer()
                    Image(systemName: "ellipsis")
                    Spacer(minLength: 40)
                    Text("4:27")
                    Spacer()
                }
                Spacer()
            }
        }
        .foregroundColor(Color.white)
        .frame(width: .infinity, height: 100)
        .background(Color("ItemColor"))
    }
}

struct TrackCard_Previews: PreviewProvider {
    static var previews: some View {
        TrackCard()
            .previewLayout(.fixed(width: 375, height: 150))
    }
}
