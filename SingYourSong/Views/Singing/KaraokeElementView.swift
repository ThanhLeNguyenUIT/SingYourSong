//
//  KaraokeElementView.swift
//  SingYourSong
//
//  Created by Tran Quoc Thinh on 30/11/2021.
//

import SwiftUI

struct KaraokeElementView: View {
//    let timer = Timer.publish(every: 1.0, on: .main, in: .common).autoconnect()
//    @EnvironmentObject var timer: Timer
    
    @EnvironmentObject var timeObject: TimingObject
    var lyricsWord: LyricsWord
    
    
    func widthParse() -> Float {
        if timeObject.currentTime <= lyricsWord.startTime {
            return 0
        } else {
            if timeObject.currentTime >= lyricsWord.endTime{
                return 1
            } else {
                return Float(timeObject.currentTime - lyricsWord.startTime) / Float((lyricsWord.endTime - lyricsWord.startTime))
            }
        }
    }
    
    var body: some View {
        ZStack{
            content
                .overlay(
                    overlayView
                        .mask(content)
                )
        }
//        .frame(maxWidth: .infinity, maxHeight: .infinity)
//        .background(Color("Background"))
        
    }
    
    private var overlayView: some View{
        GeometryReader { geometry in
            ZStack(alignment: .leading) {
                Rectangle()
                    .foregroundColor(Color.yellow)
                    .frame(width: CGFloat(self.widthParse()) * geometry.size.width)
            }
        }
    }
    
    private var content: some View{
        Text(lyricsWord.data)
            .foregroundColor(Color.white)
            .font(.system(size: 28, weight: .medium, design: .rounded))
    }
}

struct KaraokeElementView_Previews: PreviewProvider {
    static var previews: some View {
        KaraokeElementView(lyricsWord: LyricsWord(startTime: 100, endTime: 150, data: "Hellooooo"))
            .environmentObject(TimingObject())
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(Color("Background"))
    }
}
