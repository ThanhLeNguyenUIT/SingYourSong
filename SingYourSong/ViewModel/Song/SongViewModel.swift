//
//  SongViewModel.swift
//  SingYourSong
//
//  Created by Tran Quoc Thinh on 28/11/2021.
//

import Foundation

class SongViewModel: ObservableObject {
    @Published var songLyrics: [LyricsSentence] = []
    
    init(){
        loadLyrics()
    }
    
    func loadLyrics() {
        if let url = Bundle.main.url(forResource: "Chân Tình", withExtension: "json"),
           let data = try? Data(contentsOf: url){
            let decoder = JSONDecoder()
            if let jsonData = try? decoder.decode([LyricsSentence].self, from: data){
                songLyrics = jsonData
            }
        }
    }
}
