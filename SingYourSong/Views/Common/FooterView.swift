//
//  FooterView.swift
//  SingYourSong
//
//  Created by Thành Nguyễn Lê on 03/11/2021.
//

import SwiftUI
 
struct FooterView: View {
    
    
    var body: some View {
        HStack(spacing: 70){
            Button(action:{
            }){
                NavigationLink(destination: HomeView()) {
                    Image(systemName: "house.fill")
                        .font(.system(size: 25, weight: .regular))
                        .foregroundColor(.white)
                     }
                .navigationBarHidden(true)
            }
            
            Button(action:{
            }){
                NavigationLink(destination: SearchView()) {
                    Image(systemName: "magnifyingglass")
                        .font(.system(size: 25, weight: .regular))
                        .foregroundColor(.white)
                     }
                .navigationBarHidden(true)
            }
            
            Button(action:{
            }){
                NavigationLink(destination: Upload()) {
                    Image(systemName: "checkmark.icloud.fill")
                        .font(.system(size: 25, weight: .regular))
                        .foregroundColor(.white)
                     }
                .navigationBarHidden(true)
            }
            
            Button(action:{
            }){
                NavigationLink(destination: Library()) {
                    Image(systemName: "books.vertical.fill")
                        .font(.system(size: 25, weight: .regular))
                        .foregroundColor(.white)
                     }
                .navigationBarHidden(true)
            }
            
        }
        .frame(
              minWidth: 414,
              maxWidth: .infinity,
              minHeight: 40,
              maxHeight: 40,
              alignment: .bottom
            )
        .background(Color("ItemColor"))
        .edgesIgnoringSafeArea(.bottom)
    }
}

struct FooterView_Previews: PreviewProvider {
    static var previews: some View {
        FooterView()
            .preferredColorScheme(.dark)
            .previewLayout(.fixed(width: 414, height: 70))
    }
}
