//
//  UIApplication+Extension.swift
//  SingYourSong
//
//  Created by Tran Quoc Thinh on 29/10/2021.
//

import UIKit

extension UIApplication {
    func endEditting() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
