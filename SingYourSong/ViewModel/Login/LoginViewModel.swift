//
//  LoginViewModel.swift
//  SingYourSong
//
//  Created by Tran Quoc Thinh on 20/10/2021.
//

import Foundation
import SwiftUI

class LoginViewModel : ObservableObject {
//    @Published var accounts: [UserModel] = []
//
//    init() {
//        getAccounts()
//    }
//
//    func getAccounts() {
//        let accountList = [
//            UserModel(username: "thanh", password: "1234"),
//            UserModel(username: "thinh", password: "5678")
//        ]
//
//        accounts.append(contentsOf: accountList)
//    }
    
    @Published var inputAccount = UserModel()
    @Published var showProgressView = false
    
//    func validate(inputAccount: UserModel) -> Bool {
//        for account in accounts {
//            if inputAccount.username == account.username && inputAccount.password == account.password {
//                return true
//            }
//        }
//
//        return false
//    }
    
    func Login(completion: @escaping (Bool) -> Void) {
        showProgressView = true
        APIService.shared.login(inputAccount: inputAccount) { [unowned self](result: Result<Bool, APIService.APIError>) in
            showProgressView = false
            switch result {
            case .success:
                completion(true)
            case .failure:
                inputAccount = UserModel()
                completion(false)
                
            }
        }
    }
}
