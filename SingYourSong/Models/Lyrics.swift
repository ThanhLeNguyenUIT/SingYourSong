//
//  Lyrics.swift
//  SingYourSong
//
//  Created by Tran Quoc Thinh on 23/11/2021.
//

import Foundation

struct LyricsSentence: Codable, Hashable{
    let words: [LyricsWord]
}

struct LyricsWord: Codable, Hashable{
    let startTime: Int
    let endTime: Int
    let data: String
}
