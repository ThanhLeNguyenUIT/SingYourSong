//
//  PlaylistCard.swift
//  SingYourSong
//
//  Created by Thành Nguyễn Lê on 03/11/2021.
//

import SwiftUI

struct PlaylistCard: View {
    var body: some View {
        VStack{
            HStack{
                Spacer()
                Image("songbanner")
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 70, height: 70)
                    .clipped()
                Spacer()
                VStack{
                    Spacer(minLength: 10)
                    Text("Wikinson UK")
                    Spacer()
                    Text("Song Name")
                    Spacer()
                    HStack{
                        Spacer()
                        Image(systemName: "heart.fill")
                        Spacer(minLength: 95)
                    }
                    Spacer(minLength: 10)
                }
                Spacer(minLength: 170)
                VStack{
                    Spacer()
                    Image(systemName: "ellipsis")
                    Spacer(minLength: 40)
                }
                Spacer()
            }
        }
        .foregroundColor(Color.white)
        .frame(width: .infinity, height: 100)
        .background(Color("ItemColor"))
    }
}

struct PlaylistCard_Previews: PreviewProvider {
    static var previews: some View {
        PlaylistCard() .previewLayout(.fixed(width: 428, height: 150))
    }
}
