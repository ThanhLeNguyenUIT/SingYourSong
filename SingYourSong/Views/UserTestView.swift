//
//  UserTestView.swift
//  SingYourSong
//
//  Created by Thành Nguyễn Lê on 28/11/2021.
//

import SwiftUI

struct UserTestView: View {
    @ObservedObject var userTest = UserTestVM()
    
    @State var name = ""
    @State var email = ""
    @State var password = ""
    
    var body: some View {
        List(userTest.lstUser){user in
            HStack{
                Text("Name: " + user.name + " " + user.email)
                Spacer()
                
                //Update Button
                Button(action:{
                    userTest.UpdateUser(user: user)
                },label:{
                    Image(systemName: "pencil")
                })
                    .buttonStyle(BorderlessButtonStyle())
                
                //Delete Button
                Button(action:{
                    userTest.DeleteUser(user: user)
                },label:{
                    Image(systemName: "minus.circle")
                })
                    .buttonStyle(BorderlessButtonStyle())
            }
        }
        
        Spacer()
        
        VStack(spacing: 5){
            TextField("Name", text: $name)
                .textFieldStyle(RoundedBorderTextFieldStyle())
            TextField("Email", text: $email)
                .textFieldStyle(RoundedBorderTextFieldStyle())
            TextField("Password", text: $password)
                                    .textFieldStyle(RoundedBorderTextFieldStyle())
            
            Button(action:{
                // Call Add function
                userTest.AddUser(name: name, email: email, password: password)
                
                // Clear the text feild
                name = ""
                email = ""
                password = ""
                
            }, label:{
                Text("Add User")
            })
        }
    }
    
    init(){
        userTest.GetListUser()
    }
}

struct UserTestView_Previews: PreviewProvider {
    static var previews: some View {
        UserTestView()
    }
}
