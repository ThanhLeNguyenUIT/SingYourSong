//
//  APIService.swift
//  SingYourSong
//
//  Created by Tran Quoc Thinh on 28/10/2021.
//

import Foundation
import SwiftUI

class APIService {
    static let shared = APIService()
    
    
    enum APIError: Error{
        case error
    }
    
    func login(inputAccount: UserModel, completion: @escaping (Result<Bool,APIError>) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            if inputAccount.password == "1" {
                completion(.success(true))
            } else {
                completion(.failure(APIError.error))
            }
        }
    }
}
