//
//  HorizontalScroll.swift
//  SingYourSong
//
//  Created by Tran Quoc Thinh on 31/10/2021.
//

import SwiftUI

struct MusicTypeView: View {
    var songTypeModel : SongTypeModel
    
    var body: some View {
        VStack {
            Text(songTypeModel.typeName)
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.leading, 25)
                .font(Font.title2)
            
            ScrollView(.horizontal) {
                HStack{
                    ForEach(songTypeModel.songs) { song in
                        NavigationLink(destination: SongDetailView()) {
                            MusicThumbnail(Song: song)
                        }
                    }
                }
                .padding(.leading, 5)
            }
        }
        .frame(maxWidth: .infinity, maxHeight: 250, alignment: .leading)
        .background(Color("Foreground"))
        .cornerRadius(10)
        
    }
}

struct HorizontalScroll_Previews: PreviewProvider {
    static var exampleType = SongTypeModel(typeName: "Nhac tre", songs: [
        SongModel(songName: "Noi tinh yeu bat dau"),
        SongModel(songName: "Noi tinh yeu bat dau"),
        SongModel(songName: "Noi tinh yeu bat dau"),
        SongModel(songName: "Noi tinh yeu bat dau")
        
    ])
    
    static var previews: some View {
        VStack {
            MusicTypeView(songTypeModel: exampleType)
        }
        .previewLayout(.sizeThatFits)
    }
}
