//
//  Authentication.swift
//  SingYourSong
//
//  Created by Tran Quoc Thinh on 30/10/2021.
//

import SwiftUI

class Authentication: ObservableObject {
    @Published var isValidated = false
    
    func updateValidation(success: Bool) {
        withAnimation {
            isValidated = success
        }
    }
}
