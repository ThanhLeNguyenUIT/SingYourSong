//
//  SearchView.swift
//  SingYourSong
//
//  Created by Tran Quoc Thinh on 07/11/2021.
//

import SwiftUI

struct SearchView: View {
    
    @State var searchString: String = "Bui Anh Tuan"
    
    var body: some View {
        ZStack {
            
            Color("ItemColor")
                            .edgesIgnoringSafeArea(.all)
            
            VStack{
                
                HeaderView(title: "Tìm kiếm", destiantion: AnyView(HomeView()))
                                    .padding(.bottom, 5)
                                    .background(Color.black)
                
                HStack {
                    Image(systemName: "magnifyingglass")
                        .foregroundColor(Color.white)
                        .padding(.all, 6)
                    TextField("", text: $searchString)
                        .padding(.vertical, 5)
                        .foregroundColor(Color.white)
                        .font(.headline)
                }
                .background(Color("Foreground"))
                .cornerRadius(6)
                .padding()
                
                if searchString != "" {
                    VStack {
                        Text("Search your song")
                            .foregroundColor(Color.white)
                            .font(.system(size: 35, weight: .semibold, design: .default))
                        Text("Find artists, beats and albums")
                            .foregroundColor(Color.white)
                            .font(.body)
                            .padding()
                    }
                    .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
                    
                } else {
                    SearchResult()
                }
                Spacer()
                FooterView()
                                    .padding(.top, 5)
                                    .background(Color.black)
            }
            .frame(maxHeight: .infinity, alignment: .top)
        .background(Color("Background"))
            
        }
    }
}

struct SearchView_Previews: PreviewProvider {
    
    static var previews: some View {
        SearchView()
    }
}
