//
//  Login.swift
//  SingYourSong
//
//  Created by Thành Nguyễn Lê on 14/10/2021.
//

import SwiftUI

struct Login: View {
    
//    @EnvironmentObject var loginViewModel: LoginViewModel
//
//    @State var inputUsername: String = ""
//    @State var inputPassword: String = ""
    
    @StateObject private var loginVM = LoginViewModel()
    @EnvironmentObject var authentication: Authentication
    
    @State var alertTitle: String = ""
    @State var showAlert: Bool = false
    
    var body: some View {
        VStack(spacing: 20) {
            Image("WSLogo")
                .resizable()
                .frame(width: 300, height: 300)
                .cornerRadius(150)
            Spacer()
                .frame(width: 0, height: 50)
            TextField("Username", text: $loginVM.inputAccount.username)
                .padding(.horizontal)
                .frame(height: 55)
                .cornerRadius(50)
            SecureField("Password", text: $loginVM.inputAccount.password)
                .padding(.horizontal)
                .frame(height: 55)
                .cornerRadius(50)
            Button("Login") {
                loginVM.Login { success in
                    authentication.updateValidation(success: success)
                }
            }
                .foregroundColor(.white)
                .font(.headline)
                .frame(width: 170, height: 50)
                .background(Color.accentColor)
                .cornerRadius(40)
            Button("Register now", action: {
                
            })
                .font(.headline)
                
        }
        .padding(15)
        .alert(isPresented: $showAlert, content: getAlert)
        .autocapitalization(.none)
        .textFieldStyle(RoundedBorderTextFieldStyle())
    
    }
    
    func loginButtonPressed() {
        
    }
    
    func textIsAppropriate() -> Bool {
        if loginVM.inputAccount.username.count < 1 || loginVM.inputAccount.password.count < 1 {
            alertTitle = "Please insert both username and password"
            showAlert = true
            return false
        } else {
            return true
        }
    }
    
    func getAlert() -> Alert {
        return Alert(title: Text(alertTitle))
    }
}

struct Login_Previews: PreviewProvider {
    static var previews: some View {
        Login()
    }
}
