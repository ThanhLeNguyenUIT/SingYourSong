//
//  SongModel.swift
//  SingYourSong
//
//  Created by Tran Quoc Thinh on 01/11/2021.
//

import Foundation

struct SongModel: Codable, Identifiable{
    let id: String
    var songName: String
    var thumbnailLink: String
    var lyricsLink: String
    var lyrics: [LyricsSentence]?
    
    
    init(id: String = UUID().uuidString,
         songName: String = "Noi tinh yeu bat dau",
         thumbnailLink: String = "BuiAnhTuan",
         lyricsLink: String = "Chân Tình") {
        
        self.id = id;
        self.songName = songName
        self.thumbnailLink = thumbnailLink
        self.lyricsLink = lyricsLink
    }
}

struct SongTypeModel: Codable, Identifiable {
    let id: String
    var typeName: String
    var songs: [SongModel]
    
    init(id: String = UUID().uuidString, typeName: String, songs: [SongModel]){
        self.id = id
        self.typeName = typeName
        self.songs = songs
    }
}
