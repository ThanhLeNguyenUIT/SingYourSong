//
//  SearchResult.swift
//  SingYourSong
//
//  Created by Tran Quoc Thinh on 07/11/2021.
//

import SwiftUI

struct SearchResult: View {
    
    @State var selection: String = "All"
    let filterOptions = [
        "All", "Beat", "Artist", "Album"
    ]
    
    init() {
        UISegmentedControl.appearance().selectedSegmentTintColor = UIColor(red: 0.65, green: 0.65, blue: 0.65, alpha: 1)
        UISegmentedControl.appearance().backgroundColor = UIColor(red: 0.443, green: 0.443, blue: 0.443, alpha: 1)
        
        
        let attributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor.black,
            
        ]
        let attributes2: [NSAttributedString.Key: Any] = [
            .foregroundColor: UIColor.white,
            
        ]
        UISegmentedControl.appearance().setTitleTextAttributes(attributes, for: .selected)
        UISegmentedControl.appearance().setTitleTextAttributes(attributes2, for: .normal)
    }
    
    var body: some View {
        
        VStack(spacing: 0) {
            Picker(
                selection: $selection,
                content: {
                    ForEach(filterOptions.indices) { index in
                        Text(filterOptions[index])
                            .tag(filterOptions[index])
                    }
                },
                label: {
                    Text("Picker")
            })
            .pickerStyle(SegmentedPickerStyle())
            
            switch(selection) {
            case "All":
                ScrollView(.vertical, showsIndicators: true){
                    VStack(spacing: 0){
                        ForEach(filterOptions.indices) { option in
                            MusicSearchResult()
                            
                        }
                    }
                }
                .padding(.all, 0)
            default:
                Text("None")
            }
        }
    }
}

struct SearchResult_Previews: PreviewProvider {
    static var previews: some View {
        SearchResult()
    }
}
