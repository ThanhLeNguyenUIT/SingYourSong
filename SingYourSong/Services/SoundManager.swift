//
//  SoundManager.swift
//  SingYourSong
//
//  Created by Tran Quoc Thinh on 06/12/2021.
//

import Foundation
import AVKit

class SoundManager {
    static let instance = SoundManager()
    
    var player: AVAudioPlayer?
    
    func playSound(sound: String){
        guard var url = Bundle.main.url(forResource: sound, withExtension: ".mp3") else { return }
        do{
            player = try AVAudioPlayer(contentsOf: url)
            player?.play()
        } catch let error {
            print("Error playing sound:. \(error)")
        }
        
    }
    
    func pauseSound(){
        player?.pause()
    }
}
