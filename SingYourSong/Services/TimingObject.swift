//
//  TimingObject.swift
//  SingYourSong
//
//  Created by Tran Quoc Thinh on 02/12/2021.
//

import Foundation

class TimingObject: ObservableObject{
    @Published var currentTime: Int;
    @Published var timer = Timer.publish(every: 0.01, on: .main, in: .common).autoconnect()
    
    init(){
        currentTime = 0
    }
    
    func updateTime(newTime: Int){
        self.currentTime = newTime
    }
}
