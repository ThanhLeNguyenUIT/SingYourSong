//
//  Playlist.swift
//  SingYourSong
//
//  Created by Thành Nguyễn Lê on 03/11/2021.
//

import SwiftUI

struct Playlist: View {
    var body: some View {
        VStack{
            Spacer()
            HeaderView(title: "Danh sách phát", destiantion: AnyView(HomeView()), isBack: true)
                .padding(.bottom, 5)
                .background(Color.black)
            VStack{
                ScrollView(.vertical){
                    PlaylistCard()
                    PlaylistCard()
                }
            }
            .frame(
                  minWidth: 0,
                  maxWidth: .infinity,
                  minHeight: 0,
                  maxHeight: .infinity,
                  alignment: .topLeading
                )
            .background(Color("ItemColor"))
            .padding(.bottom, 5)
            .background(Color.black)
            FooterView()
        }
        .frame(
              minWidth: 0,
              maxWidth: .infinity,
              minHeight: 0,
              maxHeight: .infinity,
              alignment: .topLeading
            )
        .background(
            Color("ItemColor")
                .edgesIgnoringSafeArea(.all)
        )
    }
}

struct Playlist_Previews: PreviewProvider {
    static var previews: some View {
        Playlist()
    }
}
