//
//  Album.swift
//  SingYourSong
//
//  Created by Thành Nguyễn Lê on 02/12/2021.
//

import SwiftUI

struct Album: View {
    
    let image: String
    let title: String
    let description: String
    
    var body: some View {
        VStack{
            Image(image)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 140, height: 140)
                .clipped()
            Text(title)
            Text(description)
                .font(
                    .system(size: 15, weight: .semibold, design: .default))
        }
        .foregroundColor(Color.white)
        .background(Color("ItemColor"))
    }
}

struct Album_Previews: PreviewProvider {
    static var previews: some View {
        Album(image: "songbanner", title: "Top 50: Hip-hop", description: "Album by admin")
            .previewLayout(.fixed(width: 200, height: 200))
    }
}
