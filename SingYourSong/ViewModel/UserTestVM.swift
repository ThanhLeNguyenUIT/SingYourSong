//
//  UserTestVM.swift
//  SingYourSong
//
//  Created by Thành Nguyễn Lê on 28/11/2021.
//

import Foundation
import Firebase

class UserTestVM: ObservableObject{
    @Published var lstUser = [UserTestModel]()
    
    
    func GetListUser(){
        
        // Get reference to firebase
        let db = Firestore.firestore()
        
        // Read the document
        db.collection("User").getDocuments { snapshot, err in
            // check the error
            if err == nil {
                // no error
                
                //Update the list property in the main thread
                DispatchQueue.main.async{
                    if let snapshot = snapshot{
                        // Get all the document and create User
                        self.lstUser = snapshot.documents.map {doc in
                            
                            // Create a User item for each document returned
                            return UserTestModel(id: doc.documentID,
                                                 email: doc["Email"] as? String ?? "",
                                                 name: doc["Name"] as? String ?? "",
                                                 password: doc["Password"] as? String ?? "")
                        }
                    }
                }
            }
            else{
                // handle the error
            }
        }
    }
    
    func AddUser(name: String, email: String, password: String){
        
        // Get reference to firebase
        let db = Firestore.firestore()
        
        // Add a document to a collection
        db.collection("User").addDocument(data: ["Name": name, "Email": email, "Password": password]) { error in
            
            // check for error
            if error == nil{
                // No errors
                
                // Call to refesh data
                self.GetListUser()
            }
            else{
                //handle data
            }
        }
    }
    
    func DeleteUser(user: UserTestModel){
        // Get reference to firebase
        let db = Firestore.firestore()
        
        //Delete user
        db.collection("User").document(user.id).delete { error in
            // check for error
            if error == nil{
                // No errors
                
                // Call to refesh data
                self.GetListUser()
            }
            else{
                //handle data
            }
        }
    }
    
    func UpdateUser(user: UserTestModel){
        // Get reference to firebase
        let db = Firestore.firestore()
        
        //Update user
        
    }
}
