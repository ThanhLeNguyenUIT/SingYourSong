//
//  MusicThumbnail.swift
//  SingYourSong
//
//  Created by Tran Quoc Thinh on 01/11/2021.
//

import SwiftUI

struct MusicThumbnail: View {
    var Song: SongModel
    
    var body: some View {
        VStack{
            Image(Song.thumbnailLink)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 105, height: 150)
                .clipped()
                
            Text(Song.songName)
                .font(.headline)
                .fontWeight(.medium)
                .frame(width: 75)
        }
        .padding(.leading, 5)
    }
}

struct MusicThumbnail_Previews: PreviewProvider {
    static var exampleSong: SongModel = SongModel(songName: "Noi tinh yeu bat dau")
    
    static var previews: some View {
        MusicThumbnail(Song: exampleSong)
            .previewLayout(.sizeThatFits)
    }
}
