//
//  LibraryCard.swift
//  SingYourSong
//
//  Created by Thành Nguyễn Lê on 24/10/2021.
//

import SwiftUI

struct LibraryCard: View {
    @State var icon: String
    @State var title: String
    @State var screen: String
    var body: some View {
        HStack(){
            Image(systemName: icon)
                .padding(.leading)
            Text(title)
            Spacer()
            Image(systemName: "chevron.right")
                .padding(.trailing)
        }
        .foregroundColor(Color.white)
        .frame(width: .infinity, height: 50)
        .background(Color("ItemColor"))
    }
}

struct LibraryCard_Previews: PreviewProvider {
    static var previews: some View {
        LibraryCard(icon: "text.badge.plus", title: "Playlists & Albums", screen: "Playlists")
            .previewLayout(.fixed(width: 375, height: 150))
    }
}
