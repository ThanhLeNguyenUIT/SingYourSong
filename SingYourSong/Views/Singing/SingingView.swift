//
//  SingingView.swift
//  SingYourSong
//
//  Created by Tran Quoc Thinh on 07/11/2021.
//

import Foundation
import SwiftUI

struct SingingView: View {
    @State var songTime: Int = 300
    @State var currentTime: Double = 70
    @State var songVM = SongViewModel()
    @State var timeObject = TimingObject()
    
    @State var isSinging = false
    @State var singerVoice = false
    @State var upTone = 0
    
    func parseSecond(sec: Int) -> String {
        if sec < 10 {
            return "0" + String(sec)
        } else {
            return String(sec)
        }
    }
    
    var body: some View {
        VStack{
            HStack(spacing: 10){
                Text("\(Int(currentTime/60)):\(parseSecond(sec: Int(currentTime)%60))")
                    .font(.system(size: 20, weight: .medium, design: .default))
                    .foregroundColor(Color.white)
                
                Slider(value: $currentTime, in: 0...Double(songTime))
                    .accentColor(Color("VideoSlider"))
                
                Text("\(songTime/60):\(parseSecond(sec: songTime%60))")
                    .font(.system(size: 20, weight: .medium, design: .default))
                    .foregroundColor(Color.white)
            }
            .padding(.top, 20)
            .padding(.horizontal, 10)
            
//            KaraokeLyricsView(content: songVM.songLyrics)
            ScrollView {
                VStack {
                    ForEach(songVM.songLyrics, id: \.self){ lyricsSentence in
                        KaraokeLyricsView(content: lyricsSentence)
                    }
                }
            }
            .onReceive(timeObject.timer, perform: { _ in
                if isSinging{
                    timeObject.updateTime(newTime: timeObject.currentTime + 10)
                    currentTime = Double(timeObject.currentTime / 1000)
                }
            })
            
            HStack(spacing: 0){
                VStack{
                    //Voice
                    HStack(spacing: 0){
                        Image(systemName: "mouth.fill")
                            .foregroundColor(Color.white)
                            .font(.system(size: 30))
                            
                        
                        Text("Giong ca si")
                            .foregroundColor(Color.white)
                            .frame(width: 100)
                            .font(.system(size: 25, weight: .medium, design: .rounded))
                                                
                    }
                    .padding(.leading, 10)
                    
                    //Tone
                    HStack (spacing: 0){
                        Text("Tone:")
                            .foregroundColor(Color.white)
                            .font(.system(size: 25, weight: .medium, design: .rounded))
                            .frame(width: 65)
                        
                        VStack(spacing: 0){
                            Image(systemName: "chevron.up")
                                .foregroundColor(Color.white)
                                .frame(width: 50, height: 50)
                                .padding(.leading, 15)
                                
                            
                            Text(upTone < 0 ? "\(upTone)" : "+\(upTone)")
                                .foregroundColor(Color.white)
                                .font(.system(size: 25, weight: .medium, design: .rounded))
                                
                            Image(systemName: "chevron.down")
                                .foregroundColor(Color.white)
                                .frame(width: 50, height: 50)
                                .padding(.leading, 15)
                        }
                    }
                    
                }
                
                Button {
                    isSinging.toggle()
                    if isSinging{
                        SoundManager.instance.playSound(sound: "Chan Tinh")
                    } else {
                        SoundManager.instance.pauseSound()
                    }
                } label: {
                    isSinging ? Image(systemName: "pause.circle")
                        .font(.system(size: 100))
                        .foregroundColor(Color.red)
                    : Image(systemName: "record.circle")
                        .font(.system(size: 100))
                        .foregroundColor(Color.red)
                    
                }

                
                VStack(spacing: 40){
                    HStack(spacing: 10){
                        Image(systemName: "arrow.clockwise")
                            .foregroundColor(Color.white)
                            .font(.system(size: 30))
                        
                        Text("Bat dau lai")
                            .foregroundColor(Color.white)
                            .font(.system(size: 25, weight: .medium, design: .rounded))
                            
                    }
                    
                    HStack(spacing: 10){
                        Image(systemName: "checkmark.circle")
                            .foregroundColor(Color.white)
                            .font(.system(size: 30))
                            
                        Text("Hoan tat")
                            .foregroundColor(Color.white)
                            .font(.system(size: 25, weight: .medium, design: .rounded))
                    }
                }
                .padding(.trailing, 10)
            }
            .padding(.top, 50)
        }
        .navigationBarHidden(true)
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color("Background"))
        .environmentObject(timeObject)
    }
}

struct SingingView_Previews: PreviewProvider {
    
    static var previews: some View {
        SingingView()
    }
}
