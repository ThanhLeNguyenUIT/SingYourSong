//
//  HeaderView.swift
//  SingYourSong
//
//  Created by Thành Nguyễn Lê on 24/10/2021.
//

import SwiftUI

struct HeaderView: View {
    @State var title: String
    @State var destiantion: AnyView
    @State var isBack : Bool = false
    var body: some View {
        HStack{
            if(isBack){
                NavigationLink(destination: destiantion, label: {
                    Image(systemName: "chevron.backward")
                        .font(.system(size: 25, weight: .regular))
                        .foregroundColor(.white)
                })
                    .padding(.bottom)
                    .padding(.leading)
            }
            Spacer()
            Text(title)
                .foregroundColor(.white)
                .font(
                    .system(size: 25, weight: .bold, design: .default)
                )
                .padding(.bottom)
                .frame( alignment: .center)
               
            Spacer()
            NavigationLink(destination: destiantion, label: {
                Image(systemName: "person.crop.circle")
                    .font(.system(size: 25, weight: .regular))
                    .foregroundColor(.white)
            })
                .padding(.bottom)
                .padding(.trailing)
                
//            Button(action:{
//
//            }){
//                Image(systemName: "person.crop.circle")
//                    .font(.system(size: 20, weight: .regular))
//                    .foregroundColor(.white)
//            }
//            .padding(.bottom)
//            .padding(.trailing)
        }
        .frame(
              minWidth: 414,
              maxWidth: .infinity,
              minHeight: 50,
              maxHeight: 50,
              alignment: .top
            )
        .background(Color("ItemColor"))
    }
}

struct HeaderView_Previews: PreviewProvider {
    static var previews: some View {
        HeaderView(title: "Thư viện", destiantion: AnyView(HomeView()), isBack: false)
            .preferredColorScheme(.dark)
            .previewLayout(.fixed(width: 414, height: 70))
    }
}
