//
//  SingYourSongApp.swift
//  SingYourSong
//
//  Created by Thành Nguyễn Lê on 14/10/2021.
//

import SwiftUI
import Firebase

@main
struct SingYourSongApp: App {
    @StateObject var authentication = Authentication()
    @StateObject var songTypeViewModel = SongTypeViewModel()
    
    init(){
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            if authentication.isValidated {
                NavigationView {
                    HomeView()
                        .environmentObject(authentication)
                        .environmentObject(songTypeViewModel)
                        .navigationBarHidden(true)
                }
                .navigationBarHidden(true)
            } else {
                Login()
                    .environmentObject(authentication)
//                KaraokeElementView(lyricsWord: LyricsWord(startTime: 11820, endTime: 12070, data: "Mùa"))
//                KaraokeLyricsView(content: LyricsSentence(words: [LyricsWord]))
            }
        }
    }
}
