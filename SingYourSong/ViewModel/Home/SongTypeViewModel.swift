//
//  SongTypeViewModel.swift
//  SingYourSong
//
//  Created by Tran Quoc Thinh on 01/11/2021.
//

import Foundation

class SongTypeViewModel: ObservableObject {
    @Published var items: [SongTypeModel] = []
    
    init(){
        getItems()
    }
    func getItems() {
        let newItems = [
            SongTypeModel(typeName: "Nhac tre", songs: [
                SongModel(songName: "Noi tinh yeu bat dau"),
                SongModel(songName: "Noi tinh yeu bat dau"),
                SongModel(songName: "Noi tinh yeu bat dau"),
                SongModel(songName: "Noi tinh yeu bat dau")
            ]),
            SongTypeModel(typeName: "Nhac Bolero", songs: [
                SongModel(songName: "Noi tinh yeu bat dau"),
                SongModel(songName: "Noi tinh yeu bat dau"),
                SongModel(songName: "Noi tinh yeu bat dau"),
                SongModel(songName: "Noi tinh yeu bat dau")
            ]),SongTypeModel(typeName: "Nhac Lofi", songs: [
                SongModel(songName: "Noi tinh yeu bat dau"),
                SongModel(songName: "Noi tinh yeu bat dau"),
                SongModel(songName: "Noi tinh yeu bat dau"),
                SongModel(songName: "Noi tinh yeu bat dau")
            ])
        ]
        
        items.append(contentsOf: newItems)
    }
    
    func deleteSongType(indexSet: IndexSet) {
        items.remove(atOffsets: indexSet)
    }
    
    func addSongType(typeName: String, songs: [SongModel]) {
        let newItem = SongTypeModel(typeName: typeName, songs: songs)
        items.append(newItem)
    }
    
    func addSong(item: SongTypeModel, newSongs: [SongModel]) {
        
        if let index = items.firstIndex(where: {$0.id == item.id}) {
            //items[index] = item.updateCompletion()
            items[index].songs.append(contentsOf: newSongs)
        }
    }
    
    //saveItem
    //
}
