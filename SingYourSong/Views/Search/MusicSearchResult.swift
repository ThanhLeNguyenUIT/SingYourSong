//
//  MusicSearchResult.swift
//  SingYourSong
//
//  Created by Tran Quoc Thinh on 07/11/2021.
//

import SwiftUI

struct MusicSearchResult: View {
    var body: some View {
        HStack{
            Image("BuiAnhTuan")
                .resizable()
                .scaledToFill()
                .frame(width: 65, height: 65)
                .cornerRadius(5)
                .border(Color.white, width: 2)
            
            VStack(alignment: .leading){
                Text("Noi tinh yeu bat dau")
                    .padding(.top, 5)
                    .font(.system(size: 25, weight: .semibold, design: .default))
                    .foregroundColor(Color.white)
                
                Text("Bui Anh Tuan")
                    .padding(.vertical, 3)
                    .font(.system(size: 17, weight: .medium, design: .rounded))
                    .foregroundColor(Color.white)
            }
            .padding(.leading, 10)
        }
        .padding(.leading, 10)
        .padding(.vertical, 10)
        .frame(maxWidth: .infinity, alignment: .leading)
        .background(Color("Background"))
        
    }
}

struct MusicSearchResult_Previews: PreviewProvider {
    static var previews: some View {
        MusicSearchResult()
    }
}
