//
//  Result.swift
//  SingYourSong
//
//  Created by Thành Nguyễn Lê on 08/11/2021.
//

import SwiftUI

struct ResultView: View {
    var body: some View {
        ZStack {
            
            Color("ItemColor")
                            .edgesIgnoringSafeArea(.all)
            
            VStack{
                HeaderView(title: "Result", destiantion: AnyView(HomeView()))
                                    .padding(.bottom, 5)
                                    .background(Color.black)
                
                VStack(alignment: .leading){
                    Spacer()
                    Text("Sound")
                        .foregroundColor(Color(.white))
                    Spacer(minLength: 30)
                    HStack{
                        Image(systemName: "pause.fill")
                            .foregroundColor(Color(.white))
                        Image(systemName: "waveform")
                            .resizable()
                                        .scaledToFit()
                            .foregroundColor(Color(.white))
                            .frame(width: 350, height: 20)
                    }
                    .frame(width: 430, height: 60, alignment: .center)
                        .background(Color("BackgroundColor2"))
                    Spacer(minLength: 40)
                }
                .frame(
                      minWidth: 0,
                      maxWidth: .infinity,
                      minHeight: 0,
                      maxHeight: 170,
                      alignment: .topLeading
                    )
                .background(Color("ItemColor"))
                .padding(.bottom, 5)
                .background(Color.black)
                
                VStack(alignment: .leading, spacing: 20){
                    Text("Lyrics")
                        .foregroundColor(Color(.white))
                    HStack{
                        Spacer()
                        Text("Đoàn quân Việt Nam đi chung lòng cứu quốc, bước chân dồn vang trên đường gập ghềnh xa, cờ in máu chiến thắng mang hồn nước, súng ngoài xa chen khúc quân hành ca, đường vinh quang xây xác quân thù, thắng gian lao cùng nhau lập chiến khu, vì nhân dân chiến đấu không ngừng, tiến mau ra sa trường. Tiến lên, cùng tiến lên, nước non Việt Nam ta vững bền.")
                            .foregroundColor(Color(.white))
                        Spacer()
                    }
                    .frame(width: .infinity, height: 250, alignment: .center)
                    HStack(alignment: .center){
                        Spacer()
                        Button(action: {
                            
                        }, label: {
                            Text("Xác nhận")
                                .foregroundColor(.white)
                                .frame(width: 100.0, height: 50.0)
                                .background(Color("ButtonColor"))
                        })
                        Spacer()
                    }
                    .frame(width: .infinity, height: 50, alignment: .center)
                    Spacer()
                }
                .frame(
                      minWidth: 0,
                      maxWidth: .infinity,
                      minHeight: 0,
                      maxHeight: .infinity,
                      alignment: .topLeading
                    )
                .background(Color("ItemColor"))
                
                FooterView()
                                    .padding(.top, 5)
                                    .background(Color.black)
            }
        }
    }
}

struct Result_Previews: PreviewProvider {
    static var previews: some View {
        ResultView()
    }
}
