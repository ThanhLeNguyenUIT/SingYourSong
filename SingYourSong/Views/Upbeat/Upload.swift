//
//  Upload.swift
//  SingYourSong
//
//  Created by Thành Nguyễn Lê on 08/11/2021.
//

import SwiftUI

struct Upload: View {
    var body: some View {
        ZStack {
            
            Color("ItemColor")
                            .edgesIgnoringSafeArea(.all)
            
            VStack{
                HeaderView(title: "Tách lời và nhạc", destiantion: AnyView(HomeView()))
                    .padding(.bottom, 5)
                                      .background(Color.black)

                VStack{
                    Spacer(minLength: 200)
                    VStack{
                        Image(systemName: "doc.fill")
                            .resizable()
                                        .scaledToFit()
                                        .foregroundColor(.white)
                            .frame(width: 100.0, height: 100.0)
                        Spacer(minLength: 100)
                        Button(action: {
                            
                        }, label: {
                            Text("Browse file")
                                .foregroundColor(.white)
                                .frame(width: 100.0, height: 50.0)
                                .background(Color("ButtonColor"))
                        })
                            
                    }.background(Rectangle()
                                    .stroke(style: StrokeStyle(lineWidth: 2, dash: [5]))
                                    .foregroundColor(.white)
                                    .frame(width: 315, height: 375))
                    Spacer(minLength: 150)
                    VStack{
                        Text("Lưu ý")
                        Text("Vui lòng chọn file có đuôi .mp3")
                    }
                    .foregroundColor(.white)
                    .padding(.bottom,50)
                    Spacer()
                    FooterView()
                                        .padding(.top, 5)
                                        .background(Color.black)
                }
                .frame(
                      minWidth: 0,
                      maxWidth: .infinity,
                      minHeight: 0,
                      maxHeight: .infinity,
                      alignment: .top
                    )
                .background(Color("ItemColor"))
            }
        }
    }
}

struct Upload_Previews: PreviewProvider {
    static var previews: some View {
        Upload()
    }
}
