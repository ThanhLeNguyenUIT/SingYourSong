//
//  LikedTrack.swift
//  SingYourSong
//
//  Created by Thành Nguyễn Lê on 06/12/2021.
//

import SwiftUI

struct LikedTrack: View {
    var body: some View {
        ZStack {
            
            Color("ItemColor")
                            .edgesIgnoringSafeArea(.all)

            
            VStack{
                HeaderView(title: "Bài hát yêu thích", destiantion: AnyView(HomeView()), isBack: true)
                    .padding(.bottom, 5)
                                        .background(Color.black)

                VStack{
                    ScrollView(.vertical){
                        TrackCard()
                        TrackCard()
                    }
                }
                .frame(
                      minWidth: 0,
                      maxWidth: .infinity,
                      minHeight: 0,
                      maxHeight: .infinity,
                      alignment: .topLeading
                    )
                .background(Color("ItemColor"))
                
                FooterView()
                                    .padding(.top, 5)
                                    .background(Color.black)
            }
        }
    }
}

struct LikedTrack_Previews: PreviewProvider {
    static var previews: some View {
        LikedTrack()
    }
}
