//
//  Library.swift
//  SingYourSong
//
//  Created by Thành Nguyễn Lê on 17/10/2021.
//

import SwiftUI

struct Library: View {
    var body: some View {			
        ZStack {

            Color("ItemColor")
                .edgesIgnoringSafeArea(.all)
            
            VStack{
                HeaderView(title: "Thư viện", destiantion: AnyView(HomeView()))
                    .padding(.bottom, 5)
                    .background(Color.black)
                ScrollView(showsIndicators: false){
                    VStack(spacing:0.5){
                        ForEach(lstLibraryCard){ item in
                            NavigationLink(destination: item.destination ) {
                                LibraryCard(icon: item.icon, title: item.title, screen: "Chua co")
                            .navigationBarHidden(true)
                            }
                        }
                    }
                    .padding(.bottom, 5)
                    .background(Color.black)
                    
                    VStack(alignment: .leading){
                        Text("Recently Played")
                            .font(
                                .system(size: 30, weight: .semibold, design: .default))
                            .padding(.leading)
                        ScrollView(.horizontal, showsIndicators: false){
                            HStack{
                                // hard code
                                Album(image: "songbanner", title: "Top 50: Hip-hop", description: "Album by admin")
                                Album(image: "songbanner", title: "Top 50: Hip-hop", description: "Album by admin")
                                Album(image: "songbanner", title: "Top 50: Hip-hop", description: "Album by admin")
                            }
                            .padding(.leading)
                        }
                        HStack{
                            Spacer()
                            Text("See more recently")
                                .padding(.top)
                            Spacer()
                        }
                        
                    }
                    .frame(
                          minWidth: 0,
                          maxWidth: .infinity,
                          minHeight: 310,
                          maxHeight: 310,
                          alignment: .topLeading
                        )
                    .background(Color("ItemColor"))
                    .padding(.bottom, 5)
                    .background(Color.black)
                    
                    // Listening History
                    VStack(alignment: .leading){
                        Spacer(minLength: 10)
                        HStack{
                            Spacer(minLength: 10)
                            Text("Listening history")
                                .font(
                                    .system(size: 20, weight: .semibold, design: .default))
                            Spacer(minLength: 250)
                        }
                        ScrollView(.vertical){
                            SongCard()
                            SongCard()
                        }
                    }
                    .frame(
                          minWidth: 0,
                          maxWidth: .infinity,
                          minHeight: 310,
                          maxHeight: 310,
                          alignment: .topLeading
                        )
                }
                
                FooterView()
                    .padding(.top, 5)
                    .background(Color.black)
                
            }
            .frame(
                  minWidth: 0,
                  maxWidth: .infinity,
                  minHeight: 0,
                  maxHeight: .infinity,
                  alignment: .topLeading
        )
        }
    }
}

struct Library_Previews: PreviewProvider {
    static var previews: some View {
        Library()
            .preferredColorScheme(.dark)
    }
}
