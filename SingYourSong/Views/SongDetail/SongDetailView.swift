//
//  SongDetailView.swift
//  SingYourSong
//
//  Created by Tran Quoc Thinh on 07/11/2021.
//

import SwiftUI

struct SongDetailView: View {
    
    @State var videoTime: Double  = 300
    @State var selected: Int? = nil
    
    
    var body: some View {
        ZStack {
            
            Color("ItemColor")
                            .edgesIgnoringSafeArea(.all)
            
            VStack(spacing: 0) {
                
                HeaderView(title: "Thông tin bài hát", destiantion: AnyView(HomeView()))
                                    .padding(.bottom, 5)
                                    .background(Color.black)
                
                Image("Ve")
                    .resizable()
                    .frame(width: 315, height: 315)
                    .cornerRadius(158)
                    .padding(.top)
                    
                Spacer()
                    .frame(height: 40)
                
                Text("Ve giac mo bang nhung net nghieng dai kho")
                    .font(.subheadline)
                    .fontWeight(.semibold)
                    .foregroundColor(Color.white)
                    .multilineTextAlignment(.center)
                    .lineLimit(2)
                    .frame(width: 250, height: 50)

                HStack(spacing: 99.0) {
                    Image(systemName: "heart")
                        .resizable()
                        .frame(width: 35, height: 35)
                        .foregroundColor(Color.white)
                    
                    Image(systemName: "text.badge.plus")
                        .resizable()
                        .frame(width: 40, height: 40)
                        .foregroundColor(Color.white)
                    
                    Image(systemName: "ellipsis")
                        .resizable()
                        .frame(width: 30, height: 7)
                        .foregroundColor(Color.white)
                }
                .padding()
                
                HStack(spacing: 10){
                    Text("0:00")
                        .foregroundColor(Color.white)
                    
                    Slider(value: $videoTime)
                        .accentColor(Color("VideoSlider"))
                    
                    Text("6:00")
                        .foregroundColor(Color.white)
                }
                .padding(.horizontal, 10)
                
                HStack(spacing: 35){
                    Image(systemName: "backward.end")
                        .resizable()
                        .frame(width: 50, height: 50)
                        .foregroundColor(Color.white)
                    
                    Image(systemName: "play.fill")
                        .resizable()
                        .frame(width: 40, height: 50)
                        .foregroundColor(Color.white)
                        .padding(.leading, 10)
                    
                    Image(systemName: "forward.end")
                        .resizable()
                        .frame(width: 50, height: 50)
                        .foregroundColor(Color.white)
                }
                .padding(.top, 10)
                
                
                HStack{
                    Text("Tone:")
                        .foregroundColor(Color.white)
                        .font(.system(size: 25, weight: .medium, design: .rounded))
                        .padding(.leading, 30)
                    
                    VStack(spacing: 0){
                        Image(systemName: "chevron.up")
                            .foregroundColor(Color.white)
                            .frame(width: 50, height: 50)
                            .padding(.leading, 15)
                            
                        
                        Text("+5")
                            .foregroundColor(Color.white)
                            .font(.system(size: 25, weight: .medium, design: .rounded))
                            
                        Image(systemName: "chevron.down")
                            .foregroundColor(Color.white)
                            .frame(width: 50, height: 50)
                            .padding(.leading, 15)
                    }
                    
                    Spacer()
                    
    //                    Button("Sing") {
    //
    //                    }
    //                    .foregroundColor(Color.white)
    //                    .frame(width: 140, height: 35)
    //                    .background(Color.pink)
    //                    .cornerRadius(10)
    //                    .padding(.trailing)
    //
                    NavigationLink {
                        SingingView()
                        
                    } label: {
                        Text("Sing")
                            .foregroundColor(Color.white)
                            .frame(width: 140, height: 35)
                            .background(Color.pink)
                            .cornerRadius(10)
                            .padding(.trailing)
                        
                    }

                }
                .padding(.top, 50)
            }
            
            .navigationBarHidden(true)
            .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color("Background"))
        }
        
    }
}

struct SongDetailView_Previews: PreviewProvider {
    static var previews: some View {
        SongDetailView()
    }
}
